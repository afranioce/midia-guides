<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170501165159 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sale_invoice (id INT AUTO_INCREMENT NOT NULL, costumer_id INT NOT NULL, order_id INT NOT NULL, created_by_id INT NOT NULL, changed_by_id INT DEFAULT NULL, required_at DATETIME DEFAULT NULL, total_amount DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, changed_at DATETIME DEFAULT NULL, status SMALLINT NOT NULL, INDEX IDX_E57BD5D660B71152 (costumer_id), UNIQUE INDEX UNIQ_E57BD5D68D9F6D38 (order_id), INDEX IDX_E57BD5D6B03A8386 (created_by_id), INDEX IDX_E57BD5D6828AD0A0 (changed_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sale_invoice ADD CONSTRAINT FK_E57BD5D660B71152 FOREIGN KEY (costumer_id) REFERENCES sale_costumer (id)');
        $this->addSql('ALTER TABLE sale_invoice ADD CONSTRAINT FK_E57BD5D68D9F6D38 FOREIGN KEY (order_id) REFERENCES sale_order (id)');
        $this->addSql('ALTER TABLE sale_invoice ADD CONSTRAINT FK_E57BD5D6B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sale_invoice ADD CONSTRAINT FK_E57BD5D6828AD0A0 FOREIGN KEY (changed_by_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE sale_invoice');
    }
}
