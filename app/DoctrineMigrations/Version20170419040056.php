<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170419040056 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sale_category (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, changed_by_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, changed_at DATETIME DEFAULT NULL, status SMALLINT NOT NULL, INDEX IDX_1838F56BB03A8386 (created_by_id), INDEX IDX_1838F56B828AD0A0 (changed_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sale_costumer (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, INDEX IDX_634716D8A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sale_order (id INT AUTO_INCREMENT NOT NULL, costumer_id INT NOT NULL, created_by_id INT NOT NULL, changed_by_id INT DEFAULT NULL, delivered_at DATETIME DEFAULT NULL, required_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, changed_at DATETIME DEFAULT NULL, status SMALLINT NOT NULL, INDEX IDX_25F5CB1B60B71152 (costumer_id), INDEX IDX_25F5CB1BB03A8386 (created_by_id), INDEX IDX_25F5CB1B828AD0A0 (changed_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sale_order_item (id INT AUTO_INCREMENT NOT NULL, order_id INT NOT NULL, product_id INT NOT NULL, unit_price DOUBLE PRECISION NOT NULL, discount DOUBLE PRECISION DEFAULT NULL, total DOUBLE PRECISION DEFAULT NULL, quantity SMALLINT DEFAULT 1 NOT NULL, INDEX IDX_AFBEFB4D8D9F6D38 (order_id), INDEX IDX_AFBEFB4D4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sale_order_status_history (id INT AUTO_INCREMENT NOT NULL, order_id INT NOT NULL, customer_was_notified TINYINT(1) DEFAULT NULL, comment VARCHAR(255) DEFAULT NULL, status SMALLINT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_635406668D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sale_product (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, created_by_id INT NOT NULL, changed_by_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, unit_price DOUBLE PRECISION NOT NULL, SKU VARCHAR(40) NOT NULL, created_at DATETIME NOT NULL, changed_at DATETIME DEFAULT NULL, status SMALLINT NOT NULL, UNIQUE INDEX UNIQ_A654C63F99377A4E (SKU), INDEX IDX_A654C63F12469DE2 (category_id), INDEX IDX_A654C63FB03A8386 (created_by_id), INDEX IDX_A654C63F828AD0A0 (changed_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sale_category ADD CONSTRAINT FK_1838F56BB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sale_category ADD CONSTRAINT FK_1838F56B828AD0A0 FOREIGN KEY (changed_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sale_costumer ADD CONSTRAINT FK_634716D8A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sale_order ADD CONSTRAINT FK_25F5CB1B60B71152 FOREIGN KEY (costumer_id) REFERENCES sale_costumer (id)');
        $this->addSql('ALTER TABLE sale_order ADD CONSTRAINT FK_25F5CB1BB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sale_order ADD CONSTRAINT FK_25F5CB1B828AD0A0 FOREIGN KEY (changed_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sale_order_item ADD CONSTRAINT FK_AFBEFB4D8D9F6D38 FOREIGN KEY (order_id) REFERENCES sale_order (id)');
        $this->addSql('ALTER TABLE sale_order_item ADD CONSTRAINT FK_AFBEFB4D4584665A FOREIGN KEY (product_id) REFERENCES sale_product (id)');
        $this->addSql('ALTER TABLE sale_order_status_history ADD CONSTRAINT FK_635406668D9F6D38 FOREIGN KEY (order_id) REFERENCES sale_order (id)');
        $this->addSql('ALTER TABLE sale_product ADD CONSTRAINT FK_A654C63F12469DE2 FOREIGN KEY (category_id) REFERENCES sale_category (id)');
        $this->addSql('ALTER TABLE sale_product ADD CONSTRAINT FK_A654C63FB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sale_product ADD CONSTRAINT FK_A654C63F828AD0A0 FOREIGN KEY (changed_by_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sale_product DROP FOREIGN KEY FK_A654C63F12469DE2');
        $this->addSql('ALTER TABLE sale_order DROP FOREIGN KEY FK_25F5CB1B60B71152');
        $this->addSql('ALTER TABLE sale_order_item DROP FOREIGN KEY FK_AFBEFB4D8D9F6D38');
        $this->addSql('ALTER TABLE sale_order_status_history DROP FOREIGN KEY FK_635406668D9F6D38');
        $this->addSql('ALTER TABLE sale_order_item DROP FOREIGN KEY FK_AFBEFB4D4584665A');
        $this->addSql('DROP TABLE sale_category');
        $this->addSql('DROP TABLE sale_costumer');
        $this->addSql('DROP TABLE sale_order');
        $this->addSql('DROP TABLE sale_order_item');
        $this->addSql('DROP TABLE sale_order_status_history');
        $this->addSql('DROP TABLE sale_product');
    }
}
