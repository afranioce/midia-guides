<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170511023542 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sale_order ADD chat_thread_id INT NOT NULL');
        $this->addSql('ALTER TABLE sale_order ADD CONSTRAINT FK_25F5CB1BC47D5262 FOREIGN KEY (chat_thread_id) REFERENCES thread (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_25F5CB1BC47D5262 ON sale_order (chat_thread_id)');
        $this->addSql('ALTER TABLE sale_product ADD description VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sale_order DROP FOREIGN KEY FK_25F5CB1BC47D5262');
        $this->addSql('DROP INDEX UNIQ_25F5CB1BC47D5262 ON sale_order');
        $this->addSql('ALTER TABLE sale_order DROP chat_thread_id');
    }
}
