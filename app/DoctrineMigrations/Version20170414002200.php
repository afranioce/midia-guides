<?php

namespace Application\Migrations;

use Admin\UserBundle\Entity\Person;
use Admin\UserBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170414002200 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /** @var UserManagerInterface $userManager */
        $userManager = $this->container->get('fos_user.user_manager');

        //Usuário Admin
        /** @var User $newItem */
        $newItem = $userManager->createUser();

        // FOSUserBundle required fields
        $newItem->setUsername('admin');
        $newItem->setEmail('afranioce@gmail.com');
        $newItem->setPlainPassword('midia@sis2017');
        $newItem->addRole('ROLE_ADMIN');
        $newItem->setEnabled(true);

        $profile = new Person();
        $profile->setName('Administrador');
        $newItem->setPerson($profile);

        //Usuário Cliente
        $userManager->updateUser($newItem);

        /** @var User $newItem */
        $newItem = $userManager->createUser();

        // FOSUserBundle required fields
        $newItem->setUsername('cliente');
        $newItem->setEmail('afraniomartins@outlook.com');
        $newItem->setPlainPassword('midia@sis2017');
        $newItem->setEnabled(true);

        $profile = new Person();
        $profile->setName('Cliente Teste');
        $newItem->setPerson($profile);

        $userManager->updateUser($newItem);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
