<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170422020754 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_attachment (id INT AUTO_INCREMENT NOT NULL, project_id INT NOT NULL, name VARCHAR(255) NOT NULL, size INT NOT NULL, mime_type VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_61F9A289166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, chat_thread_id INT NOT NULL, sale_order_item_id INT NOT NULL, assigned_id INT DEFAULT NULL, created_by_id INT NOT NULL, changed_by_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, delivered_at DATETIME DEFAULT NULL, progress SMALLINT DEFAULT 0 NOT NULL, due_date DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, changed_at DATETIME DEFAULT NULL, status SMALLINT NOT NULL, UNIQUE INDEX UNIQ_2FB3D0EEC47D5262 (chat_thread_id), UNIQUE INDEX UNIQ_2FB3D0EEC5927DF1 (sale_order_item_id), INDEX IDX_2FB3D0EEE1501A05 (assigned_id), INDEX IDX_2FB3D0EEB03A8386 (created_by_id), INDEX IDX_2FB3D0EE828AD0A0 (changed_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_attachment ADD CONSTRAINT FK_61F9A289166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC47D5262 FOREIGN KEY (chat_thread_id) REFERENCES thread (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC5927DF1 FOREIGN KEY (sale_order_item_id) REFERENCES sale_order_item (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEE1501A05 FOREIGN KEY (assigned_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE828AD0A0 FOREIGN KEY (changed_by_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_attachment DROP FOREIGN KEY FK_61F9A289166D1F9C');
        $this->addSql('DROP TABLE project_attachment');
        $this->addSql('DROP TABLE project');
    }
}
