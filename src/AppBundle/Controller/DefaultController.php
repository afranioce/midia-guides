<?php

namespace AppBundle\Controller;

use Admin\SaleBundle\Entity\Category;
use Admin\SaleBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="app_homepage")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Default:index.html.twig');
    }

    /**
     * @Route("/categories", name="app_categories")
     */
    public function categoryAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Category::class)->findAll();

        return $this->render('AppBundle:Default:categories.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/products/{id}", name="app_products")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function productAction(Request $request, Category $category)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository(Product::class);
//        $products = $repo->findBy(['category', $category]);
        $products = $repo->findAll();

        return $this->render('AppBundle:Default:products.html.twig', [
            'products' => $products
        ]);
    }
}
