<?php

namespace Admin\ProjectBundle\Event;

use Admin\ProjectBundle\Entity\Project;
use Symfony\Component\EventDispatcher\Event;

class ProjectEvent extends Event
{
    const PERSIST = 'project.persist';

    /**
     * ProjectEvent constructor.
     * @param Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @var Project
     */
    public $project;

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
