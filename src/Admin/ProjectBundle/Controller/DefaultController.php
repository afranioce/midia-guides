<?php

namespace Admin\ProjectBundle\Controller;

use Admin\ProjectBundle\Entity\Attachment;
use Admin\ProjectBundle\Entity\Project;
use Admin\ProjectBundle\Form\ProjectType;
use Doctrine\ORM\EntityManager;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="project_index")
     * @param Request $request
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository(Project::class)->createQueryBuilder('p');

        $query = $qb->orderBy('p.createdAt', 'DESC')->getQuery();

        $paginator = $this->get('knp_paginator');
        $projects = $paginator->paginate(
            $query,
            $request->get('page', 1)/*page number*/,
            $request->get('limit', 50)/*limit per page*/
        );

        return $this->render('AdminProjectBundle:Default:index.html.twig', [
            'projects' => $projects
        ]);
    }

    /**
     * @Route("/my", name="project_my")
     * @param Request $request
     * @Secure(roles="ROLE_USER")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $projects = $paginator->paginate(
            $em->getRepository(Project::class)->getQueryFindByUser($this->getUser()),
            $request->get('page', 1)/*page number*/,
            $request->get('limit', 50)/*limit per page*/
        );

        return $this->render('AdminProjectBundle:Default:index.html.twig', [
            'projects' => $projects
        ]);
    }


    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}", name="project_show")
     * @Method("GET")
     * @Secure(roles="ROLE_USER")
     *
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Project $project)
    {
        return $this->render('AdminProjectBundle:Default:show.html.twig', array(
            'project' => $project
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}/messages", name="project_talk")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_USER")
     *
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function talkAction(Project $project)
    {
        $thread = $project->getTalk();

        $form = $this->container->get('fos_message.reply_form.factory')->create($thread);
        $formHandler = $this->container->get('fos_message.reply_form.handler');
        $message = $formHandler->process($form);

        if ($message) {
            $this->redirectToRoute('project_talk', ['id' => $project->getId()]);
        }

        return $this->render('AdminProjectBundle:Default:talk.html.twig', array(
            'form' => $form->createView(),
            'project' => $project,
            'thread' => $project->getTalk()
        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/{id}/edit", name="project_edit")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_USER")
     *
     * @param Request $request
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Project $project)
    {
        $editForm = $this->createForm(ProjectType::class, $project);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->get('admin_main.flash')->success('message.success.edit');

            return $this->redirectToRoute('project_edit', array('id' => $project->getId()));
        }

        return $this->render('AdminProjectBundle:Default:edit.html.twig', array(
            'project' => $project,
            'edit_form' => $editForm->createView()
        ));
    }
}
