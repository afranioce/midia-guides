<?php

namespace Admin\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Attachment
 *
 * @ORM\Table(name="project_attachment")
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="Admin\ProjectBundle\Repository\AttachmentRepository")
 */
class Attachment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="project_attachment", fileNameProperty="name")
     *
     * @var File
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=255)
     */
    private $mimeType;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Admin\ProjectBundle\Entity\Project", inversedBy="attachments", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     */
    private $project;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Attachment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return Attachment
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     *
     * @return Attachment
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @return string
     */
    public function getMimeTypeName()
    {
        $mimeTypes = array(
            'text' => [
                'text/plain',
                'application/rtf'
            ],
            'code' => [
                'text/html',
                'text/html',
                'text/html',
                'text/css',
                'text/css',
                'application/javascript',
                'application/json',
                'application/xml',
                'application/x-shockwave-flash',
                'video/x-flv'
            ],
            'image' => [
                'image/png',
                'image/jpeg',
                'image/gif',
                'image/bmp',
                'image/vnd.microsoft.icon',
                'image/tiff',
                'image/tiff',
                'image/svg+xml',
                'image/svg+xml',
                'image/vnd.adobe.photoshop',
                'application/postscript',
                'application/postscript',
                'application/postscript',
                'application/vnd.oasis.opendocument.image'
            ],
            'archive' => [
                'application/zip',
                'application/x-rar-compressed',
                'application/x-msdownload',
                'application/x-msdownload',
                'application/vnd.ms-cab-compressed'
            ],
            'audio' => [
                'audio/mpeg'
            ],
            'video' => [
                'video/quicktime',
                'video/quicktime'
            ],
            'pdf' => [
                'application/pdf'
            ],
            'word' => [
                'application/msword',
                'application/vnd.oasis.opendocument.text'
            ],
            'excel' => [
                'application/vnd.ms-excel',
                'application/vnd.oasis.opendocument.formula',
                'application/vnd.oasis.opendocument.spreadsheet'
            ],
            'powerpoint' => [
                'application/vnd.ms-powerpoint'
            ],
        );

        foreach ($mimeTypes as $name => $mimeType) {
            if (in_array($this->getMimeType(), $mimeType)) {
                return $name;
            }
        }
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return Attachment
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;

        if ($file) {
            $this->setUpdatedAt(new \DateTime());
            $this->setMimeType($file->getMimeType());
            $this->setSize($file->getSize());
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return $this
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }
}
