<?php

namespace Admin\ProjectBundle\Entity;

use Admin\ChatBundle\Entity\Thread;
use Admin\MainBundle\Model\Entity as BaseEntity;
use Admin\SaleBundle\Entity\OrderItem;
use Admin\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="Admin\ProjectBundle\Repository\ProjectRepository")
 */
class Project extends BaseEntity
{
    const STATUS_PENDING = 0x002;
    const STATUS_CANCELED = 0x003;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var Thread
     *
     * @ORM\OneToOne(targetEntity="Admin\ChatBundle\Entity\Thread", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="chat_thread_id", referencedColumnName="id", nullable=false)
     */
    private $talk;

    /**
     * @var OrderItem
     *
     * @ORM\OneToOne(targetEntity="Admin\SaleBundle\Entity\OrderItem", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="sale_order_item_id", referencedColumnName="id", nullable=false)
     */
    private $saleOrderItem;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="Admin\ProjectBundle\Entity\Attachment",
     *     mappedBy="project",
     *     cascade={"persist", "remove"}, orphanRemoval=true
     * )
     */
    private $attachments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivered_at", type="datetime", nullable=true)
     */
    private $deliveredAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="progress", type="smallint", nullable=false, options={"default":0})
     */
    private $progress = 0;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Admin\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="assigned_id", referencedColumnName="id", nullable=true)
     */
    private $assignedTo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="due_date", type="datetime", nullable=true)
     */
    private $dueDate;


    public function __construct()
    {
        $this->attachments = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Thread
     */
    public function getTalk()
    {
        return $this->talk;
    }

    /**
     * @param Thread $talk
     * @return Project
     */
    public function setTalk($talk)
    {
        $this->talk = $talk;

        return $this;
    }

    /**
     * @return OrderItem
     */
    public function getSaleOrderItem()
    {
        return $this->saleOrderItem;
    }

    /**
     * @param OrderItem $saleOrderItem
     * @return Project
     */
    public function setSaleOrderItem($saleOrderItem)
    {
        $this->saleOrderItem = $saleOrderItem;

        return $this;
    }

    /**
     * @param Attachment $attachment
     * @return $this
     */
    public function addAttachment(Attachment $attachment)
    {
        $this->attachments->add($attachment);
        $attachment->setProject($this);

        return $this;
    }

    /**
     * @param Attachment $attachment
     * @return Project
     */
    public function removeAttachment(Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveredAt()
    {
        return $this->deliveredAt;
    }

    /**
     * @param \DateTime $deliveredAt
     * @return Project
     */
    public function setDeliveredAt($deliveredAt)
    {
        $this->deliveredAt = $deliveredAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @param int $progress
     * @return Project
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * @return User
     */
    public function getAssignedTo()
    {
        return $this->assignedTo;
    }

    /**
     * @param User $assignedTo
     *
     * @return $this
     */
    public function setAssignedTo(User $assignedTo = null)
    {
        $this->assignedTo = $assignedTo;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     * @return $this
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getStatusList()
    {
        return [
                Project::STATUS_PENDING => 'Pending',
                Project::STATUS_CANCELED => 'Canceled'
            ] + parent::getStatusList();
    }
}
