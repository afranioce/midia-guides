<?php

namespace Admin\ProjectBundle\EventListener;

use Admin\MainBundle\Event\ConfigureMenuEvent;

class ConfigureMenuListener
{
    /**
     * @param \Admin\MainBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu->addChild($event->getFactory()->createItem('myprojects', ['route' => 'project_my'])
            ->setAttributes(['icon' => 'fa fa-cart']));

        $menu->addChild($event->getFactory()->createItem('projects', ['route' => 'project_index'])
            ->setAttributes(['icon' => 'fa fa-cart']));
    }
}
