<?php

namespace Admin\ProjectBundle\EventListener;

use Admin\ProjectBundle\Entity\Project;
use Admin\ProjectBundle\Event\ProjectEvent;
use Admin\SaleBundle\Entity\OrderItem;
use Admin\SaleBundle\Event\OrderEvent;
use Admin\UserBundle\Entity\User;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig_Environment;

class ProjectPersistSubscriber implements EventSubscriberInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * OrderSubscriber constructor.
     * @param Twig_Environment $twig
     * @param Swift_Mailer $mailer
     */
    public function __construct(Twig_Environment $twig, Swift_Mailer $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            OrderEvent::PAID => 'onOrderPaid',
            OrderEvent::CANCELED => 'onOrderCanceled',
            ProjectEvent::PERSIST => 'onProjectCreated'
        ];
    }

    public function onOrderPaid(OrderEvent $event)
    {
        $event->getOrder()->getItems()->map(function (OrderItem $item) {
            $project = new Project();
            $project->setStatus(Project::STATUS_ACTIVE);
            $project->setName($item->getProduct()->getName());
            $project->setSaleOrderItem($item);
            $project->setCreatedBy($item->getOrder()->getCostumer()->getUser());
            $project->setDescription('');

            $threadBuilder = $this->container->get('fos_message.composer')->newThread();
            $threadBuilder
                ->addRecipient($item->getOrder()->getCostumer()->getUser())
                ->setSubject($project->getName())
                ->setBody('Projeto criado com sucesso!');

            //Enviando para os usuário admin
            $adminAccounts = $this->container->get('doctrine')->getRepository(User::class)->findByRoles(['ROLE_ADMIN']);
            foreach ($adminAccounts as $account) {
                $threadBuilder->addRecipient($account);
            }

            $sender = $this->container->get('fos_message.sender');
            $sender->send($threadBuilder->getMessage());

            $project->setTalk($threadBuilder->getMessage()->getThread());

            $em = $this->container->get('doctrine.orm.entity_manager');
            $em->persist($project);

            $event = new ProjectEvent($project);
            $this->container->get('event_dispatcher')->dispatch(ProjectEvent::PERSIST, $event);
        });
    }

    /**
     * @param ProjectEvent $event
     */
    public function onProjectCreated(ProjectEvent $event)
    {
        $project = $event->getProject();

        // creating the ACL
//        $aclProvider = $this->container->get('security.acl.provider');
//        $objectIdentity = ObjectIdentity::fromDomainObject($project);
//        $acl = $aclProvider->createAcl($objectIdentity);
//
//        $securityIdentity = UserSecurityIdentity::fromAccount($project->getCreatedBy());
//
//        $builder = new MaskBuilder();
//        $builder
//            ->add(MaskBuilder::MASK_VIEW)
//            ->add(MaskBuilder::MASK_EDIT);
//
//        // grant owner access
//        $acl->insertObjectAce($securityIdentity, $builder->get());
//        $aclProvider->updateAcl($acl);

        //Send e-mail to client
        $message = Swift_Message::newInstance()
            ->setSubject('Novo projeto ' . $project->getName())
            ->setFrom($this->container->getParameter('mailer_noreply'), $this->container->getParameter('site_name'))
            ->setTo($project->getSaleOrderItem()->getOrder()->getCostumer()->getUser()->getEmail())
            ->setBcc($this->container->getParameter('mailer_bcc_tester'))
            ->setContentType('text/html')
            ->setBody($this->twig->render('AdminProjectBundle:Emails:project_new.html.twig', [
                'project' => $project
            ], 'text/html'));

        $this->mailer->send($message);

        //Send e-mail to admin
        $message = Swift_Message::newInstance()
            ->setSubject('Novo projeto aprovado ' . $project->getName())
            ->setFrom($this->container->getParameter('mailer_noreply'), $this->container->getParameter('site_name'))
            ->setBcc($this->container->getParameter('mailer_bcc_tester'))
            ->setContentType('text/html')
            ->setBody($this->twig->render('AdminProjectBundle:Emails:project_new_admin.html.twig', [
                'project' => $project
            ], 'text/html'));

        $adminUsers = $this->container->get('doctrine')->getRepository(User::class)
            ->findByRoles(['ROLE_ADMIN']);

        foreach ($adminUsers as $adminUser) {
            $message->addTo($adminUser->getEmail());
        }

        $this->mailer->send($message);
    }

    public function onOrderCanceled(OrderEvent $event)
    {
        $event->getOrder()->getItems()->map(function (OrderItem $item) {
            $em = $this->container->get('doctrine')->getManager();
            $project = $em->getRepository(Project::class)->findOneBy(['saleOrderItem' => $item]);

            if ($project) {
                $project->setStatus(Project::STATUS_CANCELED);

                $em->merge($project);
                $em->flush();
            }
        });
    }
}
