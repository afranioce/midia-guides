<?php

namespace Admin\ChatBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AdminChatBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSMessageBundle';
    }
}
