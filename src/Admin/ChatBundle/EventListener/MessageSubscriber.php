<?php

namespace Admin\ChatBundle\EventListener;

use Admin\UserBundle\Entity\User;
use FOS\MessageBundle\Event\FOSMessageEvents;
use FOS\MessageBundle\Event\MessageEvent;
use Swift_Mailer;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig_Environment;

class MessageSubscriber implements EventSubscriberInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * OrderSubscriber constructor.
     * @param Twig_Environment $twig
     * @param Swift_Mailer $mailer
     */
    public function __construct(Twig_Environment $twig, Swift_Mailer $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSMessageEvents::POST_SEND => 'onSendMessage'
        ];
    }

    public function onSendMessage(MessageEvent $event)
    {
        /** @var User $sender */
        $sender = $event->getMessage()->getSender();

        //Send e-mail to client
        $message = \Swift_Message::newInstance()
            ->setSubject('Nova mensagem de ' . $sender->getRealname())
            ->setFrom($this->container->getParameter('mailer_noreply'), $this->container->getParameter('site_name'))
            ->setBcc($this->container->getParameter('mailer_bcc_tester'))
            ->setContentType('text/html')
            ->setBody($this->twig->render('AdminChatBundle:Emails:message.html.twig', [
                'message' => $event->getMessage()
            ], 'text/html'));

        /** @var User $participant */
        foreach ($event->getThread()->getParticipants() as $participant) {
            if (!$event->getMessage()->isReadByParticipant($participant)) {
                $message->addTo($participant->getEmail());
            }
        }

        $this->mailer->send($message);
    }
}
