<?php

namespace Admin\ChatBundle\EventListener;

use Admin\MainBundle\Event\ConfigureMenuEvent;

class ConfigureMenuListener
{
    /**
     * @param \Admin\MainBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $userMenu = $event->getFactory()->createItem('Mensagens')
            ->setAttributes(['icon' => 'fa fa-cart']);

        $userMenu->addChild($event->getFactory()
            ->createItem('Caixa de entrada', ['route' => 'fos_message_inbox']));
        $userMenu->addChild($event->getFactory()
            ->createItem('Enviados', ['route' => 'fos_message_sent']));
        $userMenu->addChild($event->getFactory()
            ->createItem('Novo', ['route' => 'fos_message_thread_new']));
        $userMenu->addChild($event->getFactory()
            ->createItem('Excluído', ['route' => 'fos_message_deleted']));

        $menu->addChild($userMenu);
    }
}
