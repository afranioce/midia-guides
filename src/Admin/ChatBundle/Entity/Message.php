<?php

namespace Admin\ChatBundle\Entity;

use Admin\UserBundle\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\MessageBundle\Entity\Message as BaseMessage;

/**
 * @ORM\Entity
 */
class Message extends BaseMessage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *   targetEntity="Admin\ChatBundle\Entity\Thread",
     *   inversedBy="messages"
     * )
     * @var User
     */
    protected $thread;

    /**
     * @ORM\ManyToOne(targetEntity="Admin\UserBundle\Entity\User")
     * @var User
     */
    protected $sender;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Admin\ChatBundle\Entity\MessageMetadata",
     *   mappedBy="message",
     *   cascade={"all"}
     * )
     * @var MessageMetadata[]|Collection
     */
    protected $metadata;
}
