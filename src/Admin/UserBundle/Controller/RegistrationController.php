<?php

namespace Admin\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseRegistrationController;
use FOS\UserBundle\Form\Factory\FactoryInterface;

class RegistrationController extends BaseRegistrationController
{
    public function registerPartialAction()
    {
        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();
        return $this->render('@FOSUser/Registration/register_content.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
