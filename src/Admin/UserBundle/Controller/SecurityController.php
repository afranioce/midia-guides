<?php

namespace Admin\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends BaseSecurityController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function loginPartialAction(Request $request)
    {
        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('@FOSUser/Security/login_content.html.twig', [
            'last_username' => null,
            'error' => null,
            'csrf_token' => $csrfToken
        ]);
    }
}
