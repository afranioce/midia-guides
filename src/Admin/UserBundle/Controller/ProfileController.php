<?php

namespace Admin\UserBundle\Controller;

use Admin\ProjectBundle\Entity\Project;
use Admin\SaleBundle\Entity\Order;
use FOS\UserBundle\Controller\ProfileController as BaseProfileController;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class ProfileController extends BaseProfileController
{
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->getDoctrine()->getManager();
        /** @var Order[] $orders */
        $orders = $em->getRepository(Order::class)->findByCostumer($user, 5, 'DESC');

        /** @var Project[] $projects */
        $projects = $em->getRepository(Project::class)->getQueryFindByUser($user)
            ->setMaxResults(5)
            ->getResult();

        return $this->render('@FOSUser/Profile/show.html.twig', array(
            'user' => $user,
            'orders' => $orders,
            'projects' => $projects
        ));
    }
}
