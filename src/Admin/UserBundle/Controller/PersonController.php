<?php

namespace Admin\UserBundle\Controller;

use Admin\UserBundle\Entity\Person;
use Admin\UserBundle\Form\PersonType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Person controller.
 *
 * @Route("person")
 */
class PersonController extends Controller
{
    /**
     * Creates a new person entity.
     *
     * @Route("/new", name="person_personal_new")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_USER")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();

            $this->get('admin_main.flash')->success('message.success.edit');

            return $this->redirectToRoute('fos_user_group_edit');
        }

        return $this->render('@AdminUser/Person/new.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a person entity.
     *
     * @Route("/", name="person_personal_show")
     * @Method("GET")
     * @Secure(roles="ROLE_USER")
     *
     * @param Person $person
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Person $person)
    {
        return $this->render('@AdminUser/Person/show.html.twig', [
            'person' => $person
        ]);
    }

    /**
     * Displays a form to edit an existing person entity.
     *
     * @Route("/edit", name="person_personal_edit")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_USER")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @internal param Person $person
     */
    public function editAction(Request $request)
    {
        $person = $this->getUser()->getPerson();

        if (!$person instanceof Person) {
            return $this->redirectToRoute('person_personal_new');
        }

        $editForm = $this->createForm(PersonType::class, $person);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->get('admin_main.flash')->success('message.success.edit');

            return $this->redirectToRoute('person_personal_edit');
        }

        return $this->render('@AdminUser/Person/edit.html.twig', [
            'person' => $person,
            'edit_form' => $editForm->createView()
        ]);
    }
}
