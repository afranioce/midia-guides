<?php

namespace Admin\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\MessageBundle\Model\ParticipantInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User.
 *
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Admin\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser implements ParticipantInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Regex(
     *   pattern="/(?=.*[A-Z])(?=.*[0-9]).{4,8}/",
     *   message="A senha deve ter no mínimo uma letra maiúscula e um número, contendo 4 a 8 caracteres."
     * )
     *
     * @var string
     */
    protected $plainPassword;

    /**
     * @ORM\OneToOne(targetEntity="Admin\UserBundle\Entity\Person", mappedBy="user", cascade={"persist"})
     */
    private $person;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set person.
     *
     * @param Person $person
     *
     * @return User
     */
    public function setPerson(Person $person = null)
    {
        $person->setUser($this);
        $this->person = $person;

        return $this;
    }

    /**
     * Get person.
     *
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Get realname.
     *
     * @return string
     */
    public function getRealname()
    {
        $realname = $this->getUsername();

        if ($this->person instanceof Person && !empty($this->person->getName())) {
            $realname = $this->person->getName();
        }

        return $realname;
    }
}
