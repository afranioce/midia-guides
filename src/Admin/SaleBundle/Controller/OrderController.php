<?php

namespace Admin\SaleBundle\Controller;

use Admin\SaleBundle\Entity\Order;
use Admin\SaleBundle\Form\OrderType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Order controller.
 *
 * @Route("order")
 */
class OrderController extends Controller
{
    /**
     * Lists all order entities.
     *
     * @Route("/", name="order_index")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $criteria = $request->get('criteria');
        $value = $request->get('value');

        $qb = $em->createQueryBuilder()
            ->select('o')
            ->from('AdminSaleBundle:Order', 'o');

        if (!empty($criteria) && !empty($value)) {
            $operator = $request->get('operator');

            $type = $em->getClassMetadata('AdminSaleBundle:Order')->getTypeOfField($criteria);
            if ($type == 'date' || $type == 'datetime') {
                $value = \DateTime::createFromFormat('d/m/Y H:i:s', $value);
            }
            if ($operator === 'like') {
                $value = $qb->expr()->literal('%' . $value . '%');
                $qb->andWhere($qb->expr()->{$operator}('o.' . $criteria, $value));
            } else {
                $qb->andWhere($qb->expr()->{$operator}('o.' . $criteria, '?0'));
                $qb->setParameter(0, $value);
            }
        }

        $paginator = $this->get('knp_paginator');
        $orders = $paginator->paginate(
            $qb->getQuery(),
            $request->get('page', 1)/*page number*/,
            $request->get('limit', 50)/*limit per page*/
        );

        return $this->render('AdminSaleBundle:Order:index.html.twig', array(
            'orders' => $orders
        ));
    }

    /**
     * @Route("/my", name="order_my")
     * @Method("GET")
     * @Secure(roles="ROLE_USER")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $criteria = $request->get('criteria');
        $value = $request->get('value');

        $qb = $em->getRepository(Order::class)->queryFindByCostumer($this->getUser());

        if (!empty($criteria) && !empty($value)) {
            $operator = $request->get('operator');

            $type = $em->getClassMetadata('AdminSaleBundle:Order')->getTypeOfField($criteria);
            if ($type == 'date' || $type == 'datetime') {
                $value = \DateTime::createFromFormat('d/m/Y H:i:s', $value);
            }
            if ($operator === 'like') {
                $value = $qb->expr()->literal('%' . $value . '%');
                $qb->andWhere($qb->expr()->{$operator}('o.' . $criteria, $value));
            } else {
                $qb->andWhere($qb->expr()->{$operator}('o.' . $criteria, '?0'));
                $qb->setParameter(0, $value);
            }
        }

        $paginator = $this->get('knp_paginator');
        $orders = $paginator->paginate(
            $qb->getQuery(),
            $request->get('page', 1)/*page number*/,
            $request->get('limit', 50)/*limit per page*/
        );

        return $this->render('AdminSaleBundle:Order:index.html.twig', array(
            'orders' => $orders
        ));
    }

    /**
     * Finds and displays a order entity.
     *
     * @Route("/{id}", name="order_show")
     * @Method("GET")
     * @Secure(roles="ROLE_USER")
     *
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Order $order)
    {
        return $this->render('AdminSaleBundle:Order:show.html.twig', array(
            'order' => $order,
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}/messages", name="order_talk")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_USER")
     *
     * @param Order $project
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function talkAction(Order $project)
    {
        $thread = $project->getTalk();

        $form = $this->container->get('fos_message.reply_form.factory')->create($thread);
        $formHandler = $this->container->get('fos_message.reply_form.handler');
        $message = $formHandler->process($form);

        if ($message) {
            $this->redirectToRoute('order_talk', ['id' => $project->getId()]);
        }

        return $this->render('AdminProjectBundle:Default:talk.html.twig', array(
            'form' => $form->createView(),
            'project' => $project,
            'thread' => $project->getTalk()
        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/{id}/edit", name="order_edit")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Order $order)
    {
        $editForm = $this->createForm(OrderType::class, $order);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            //Recalculando o valor total do pedido
            $totalAmmount = 0;
            foreach ($order->getItems() as $item) {
                $item->setTotal($item->getUnitPrice() * $item->getQuantity());
                $totalAmmount += $item->getTotal();
            }
            $order->setTotalAmount($totalAmmount);

            $this->getDoctrine()->getManager()->flush();
            $this->get('admin_main.flash')->success('message.success.edit');

            return $this->redirectToRoute('order_edit', array('id' => $order->getId()));
        }

        return $this->render('AdminSaleBundle:Order:edit.html.twig', array(
            'order' => $order,
            'edit_form' => $editForm->createView()
        ));
    }
}
