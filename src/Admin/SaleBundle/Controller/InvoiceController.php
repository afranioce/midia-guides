<?php

namespace Admin\SaleBundle\Controller;

use Admin\SaleBundle\Entity\Invoice;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Invoice controller.
 *
 * @Route("invoice")
 */
class InvoiceController extends Controller
{
    /**
     * Lists all invoice entities.
     *
     * @Route("/", name="invoice_index")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $criteria = $request->get('criteria');
        $value = $request->get('value');

        $qb = $em->createQueryBuilder()
            ->select('i')
            ->from('AdminSaleBundle:Invoice', 'i');

        if (!empty($criteria) && !empty($value)) {
            $operator = $request->get('operator');

            $type = $em->getClassMetadata('AdminSaleBundle:Invoice')->getTypeOfField($criteria);
            if ($type == 'date' || $type == 'datetime') {
                $value = \DateTime::createFromFormat('d/m/Y H:i:s', $value);
            }
            if ($operator === 'like') {
                $value = $qb->expr()->literal('%' . $value . '%');
                $qb->andWhere($qb->expr()->{$operator}('i.' . $criteria, $value));
            } else {
                $qb->andWhere($qb->expr()->{$operator}('i.' . $criteria, '?0'));
                $qb->setParameter(0, $value);
            }
        }

        $paginator = $this->get('knp_paginator');
        $invoices = $paginator->paginate(
            $qb->getQuery(),
            $request->get('page', 1)/*page number*/,
            $request->get('limit', 50)/*limit per page*/
        );

        $delete_forms = array_map(function (Invoice $invoice) {
            return $this->createDeleteForm($invoice)->createView();
        }, $invoices->getItems());

        return $this->render('@AdminSale/Invoice/index.html.twig', array(
            'invoices' => $invoices,
            'delete_forms' => $delete_forms,
        ));
    }

    /**
     * Finds and displays a invoice entity.
     *
     * @Route("/{id}", name="invoice_show")
     * @Method("GET")
     * @Secure(roles="ROLE_USER")
     *
     * @param Invoice $invoice
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Invoice $invoice)
    {
        return $this->render('@AdminSale/Invoice/show.html.twig', array(
            'invoice' => $invoice,
        ));
    }
}
