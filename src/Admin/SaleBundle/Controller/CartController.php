<?php

namespace Admin\SaleBundle\Controller;

use Admin\SaleBundle\Model\CartItem;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/cart")
 */
class CartController extends Controller
{
    /**
     * Cart Session Bag
     */
    const CART = '_cart';

    /**
     * @Route("/", name="cart")
     * @Method({"GET"})
     *
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $cart = $this->getCart();

        $total = array_reduce($cart, function ($res, CartItem $a) {
            return $res + $a->getTotal();
        });

        return $this->render('AdminSaleBundle:Cart:index.html.twig', [
            'cart' => $cart,
            'total' => $total
        ]);
    }

    /**
     * @Route("/add/{SKU}", name="cart_add")
     * @Method({"GET", "POST"})
     *
     * @param $SKU
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @TODO Está adicionando valor dobrado
     */
    public function addAction($SKU)
    {
        // fetch the cart
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('AdminSaleBundle:Product')->findOneBySKU($SKU);

        $cart = $this->getCart();

        // check if the $id already exists in it.
        if (!$product) {
            $this->get('admin_main.flash')->danger('Este produto não está disponível');
        } else {
            if (isset($cart[$SKU])) {
                $cart[$SKU]->setQuantity($cart[$SKU]->getQuantity() + 1);
            } else {
                $cart[$SKU] = new CartItem($product);
            }
            $this->setCart($cart);
        }
        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/remove/{SKU}", name="cart_remove")
     * @Method({"GET", "POST"})
     *
     * @param $SKU
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction($SKU)
    {
        $cart = $this->getCart();

        // if it doesn't exist redirect to cart index page. end
        if (!$cart) {
            $this->redirectToRoute('cart');
        }

        // check if the $id already exists in it.
        if (isset($cart[$SKU])) {
            unset($cart[$SKU]);
        } else {
            return $this->redirectToRoute('cart');
        }

        $this->setCart($cart);

        $this->get('admin_main.flash')->success('Este produto foi removido');
        return $this->redirectToRoute('cart');
    }

    /**
     * Get the cart from the session
     * @return CartItem[]
     */
    private function getCart()
    {
        return $this->container->get('session')->get(self::CART, []);
    }

    /**
     * @param CartItem[] $cart
     */
    private function setCart(array $cart = array())
    {
        $this->container->get('session')->set(self::CART, $cart);
    }
}
