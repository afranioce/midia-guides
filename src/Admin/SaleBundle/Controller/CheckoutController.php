<?php

namespace Admin\SaleBundle\Controller;

use Admin\SaleBundle\Entity\Costumer;
use Admin\SaleBundle\Entity\Order;
use Admin\SaleBundle\Entity\OrderItem;
use Admin\SaleBundle\Entity\Payment;
use Admin\SaleBundle\Event\OrderEvent;
use Admin\SaleBundle\Model\CartItem;
use Admin\UserBundle\Entity\User;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Payum\Core\Request\GetHumanStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

/**
 * Class CheckoutController
 * @package Admin\SaleBundle\Controller
 *
 * @Route("/checkout")
 */
class CheckoutController extends Controller
{
    /**
     * @Route("/step-1", name="checkout_step_one")
     */
    public function stepOneAction(Request $request)
    {
        if ($this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('checkout_step_two');
        }

        return $this->render('AdminSaleBundle:Checkout:step_one.html.twig');
    }

    /**
     * @Route("/step-2", name="checkout_step_two")
     * @Secure(roles="ROLE_USER")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function stepTwoAction(Request $request)
    {
        /** @var CartItem[] $cart */
        $cart = $request->getSession()->get('_cart');

        if (!$cart) {
            return $this->redirectToRoute('cart');
        }

        $em = $this->getDoctrine()->getManager();
        $order = new Order();

        $costumer = $em->getRepository('AdminSaleBundle:Costumer')->findOneBy(['user' => $this->getUser()]);

        $order->setStatus(Order::STATUS_HOLD);

        if (!$costumer) {
            $costumer = new Costumer();
            $costumer->setUser($this->getUser());
        }

        $order->setCostumer($costumer);

        $totalAmount = 0;

        foreach ($cart as $SKU => $cartItem) {
            $orderItem = new OrderItem();

            $orderItem->setQuantity($cartItem->getQuantity());

            //@todo Não deixa salvar com o produto vindo da sessão, n sei pq :'(
            //$orderItem->setProduct($cartItem->getProduct());
            $product = $em->getRepository('AdminSaleBundle:Product')->findOneBySKU(['SKU' => $SKU]);

            $orderItem->setProduct($product);
            $orderItem->setUnitPrice($cartItem->getUnitPrice());
            $orderItem->setTotal($cartItem->getTotal());
            $orderItem->setOrder($order);

            $order->addItem($orderItem);

            $totalAmount += $orderItem->getTotal();
        }

        $order->setTotalAmount($totalAmount);

        //@todo criar evento para criar mensagem
        //Enviando para os usuários admin
        //Criando mensagem para poder manter contato antes de criar o projeto
        $adminAccount = $this->getDoctrine()->getRepository(User::class)->findOneByRole('ROLE_ADMIN');
        $threadBuilder = $this->container->get('fos_message.composer')->newThread();
        $threadBuilder
            ->addRecipient($order->getCostumer()->getUser())
            ->setSubject('Pedido de ' . $order->getCostumer()->getUser()->getRealname())
            ->setSender($adminAccount)
            ->setBody('Um email foi enviado com os dados do pedido, em breve entraremos em contato.');

        $sender = $this->container->get('fos_message.sender');

        $sender->send($threadBuilder->getMessage());

        $order->setTalk($threadBuilder->getMessage()->getThread());

        $em->persist($order);
        $em->flush();

        // creating the ACL
        $aclProvider = $this->container->get('security.acl.provider');
        $objectIdentity = ObjectIdentity::fromDomainObject($order);
        $acl = $aclProvider->createAcl($objectIdentity);

        $securityIdentity = UserSecurityIdentity::fromAccount($order->getCreatedBy());

        // grant owner access
        $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_VIEW);
        $aclProvider->updateAcl($acl);

        $this->get('admin_main.flash')->success('Seu pedido foi criado, um email foi enviado com os dados do pedido,
        em breve entraremos em contato.');

        $event = new OrderEvent($order);
        $this->get('event_dispatcher')->dispatch(OrderEvent::EMAIL_CREATE, $event);

        //Limpando a sessão do carrinho
        $request->getSession()->remove('_cart');

        return $this->redirectToRoute('order_show', ['id' => $order->getId()]);
    }

    /**
     * @Route("/step-3/{id}", name="checkout_step_tree")
     * @Secure(roles="ROLE_USER")
     *
     * @param Request $request
     * @param Order $order
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function stepTreeAction(Request $request, Order $order)
    {
        $gatewayName = 'express_checkout';

        $storage = $this->get('payum')->getStorage(Payment::class);

        /** @var Payment $payment */
        $payment = $storage->create();
        $payment->setNumber($order->getId());
        $payment->setCurrencyCode($this->getParameter('currency'));
        $payment->setTotalAmount($order->getTotalAmount() * 100);
        $payment->setDescription('Compra de serviço em MidiaGuides');
        $payment->setClientId($order->getCostumer()->getUser()->getRealname());
        $payment->setClientEmail($order->getCostumer()->getUser()->getEmail());

        $storage->update($payment);

        $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
            $gatewayName,
            $payment,
            'checkout_done' // the route to redirect after capture
        );

        return $this->redirect($captureToken->getTargetUrl());
    }

    /**
     * @Route("/done", name="checkout_done")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doneAction(Request $request)
    {
        $token = $this->get('payum')->getHttpRequestVerifier()->verify($request);
        $gateway = $this->get('payum')->getGateway($token->getGatewayName());

        $gateway->execute($status = new GetHumanStatus($token));
        /** @var Payment $payment */
        $payment = $status->getFirstModel();

        $order = $this->getDoctrine()->getRepository(Order::class)->find($payment->getNumber());

        if (!$order) {
            $this->get('payum')->getHttpRequestVerifier()->invalidate($token);
            $this->createNotFoundException();
        }

        return $this->redirectToRoute('order_show', ['id' => $order->getId()]);
    }
}
