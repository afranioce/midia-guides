<?php

namespace Admin\SaleBundle\Controller;

use Admin\SaleBundle\Entity\Product;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Product controller.
 *
 * @Route("product")
 */
class ProductController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/", name="product_index")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $criteria = $request->get('criteria');
        $value = $request->get('value');

        $qb = $em->createQueryBuilder()
            ->select('p')
            ->from('AdminSaleBundle:Product', 'p');

        if (!empty($criteria) && !empty($value)) {
            $operator = $request->get('operator');

            $type = $em->getClassMetadata('AdminSaleBundle:Product')->getTypeOfField($criteria);
            if ($type == 'date' || $type == 'datetime') {
                $value = \DateTime::createFromFormat('d/m/Y H:i:s', $value);
            }
            if ($operator === 'like') {
                $value = $qb->expr()->literal('%' . $value . '%');
                $qb->andWhere($qb->expr()->{$operator}('p.' . $criteria, $value));
            } else {
                $qb->andWhere($qb->expr()->{$operator}('p.' . $criteria, '?0'));
                $qb->setParameter(0, $value);
            }
        }

        $paginator = $this->get('knp_paginator');
        $products = $paginator->paginate(
            $qb->getQuery(),
            $request->get('page', 1)/*page number*/,
            $request->get('limit', 50)/*limit per page*/
        );

        $delete_forms = array_map(function (Product $product) {
            return $this->createDeleteForm($product)->createView();
        }, $products->getItems());

        return $this->render('AdminSaleBundle:Product:index.html.twig', array(
            'products' => $products,
            'delete_forms' => $delete_forms,
        ));
    }

    /**
     * Creates a new product entity.
     *
     * @Route("/new", name="product_new")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm('Admin\SaleBundle\Form\ProductType', $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->get('admin_main.flash')->success('message.success.new');

            return $this->redirectToRoute('product_show', array('id' => $product->getId()));
        }

        return $this->render('AdminSaleBundle:Product:new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}", name="product_show")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('AdminSaleBundle:Product:show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/{id}/edit", name="product_edit")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);
        $editForm = $this->createForm('Admin\SaleBundle\Form\ProductType', $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->get('admin_main.flash')->success('message.success.edit');

            return $this->redirectToRoute('product_edit', array('id' => $product->getId()));
        }

        return $this->render('AdminSaleBundle:Product:edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a product entity.
     *
     * @Route("/{id}", name="product_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Product $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();

            $this->get('admin_main.flash')->success('message.success.delete');
        }

        return $this->redirectToRoute('product_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
