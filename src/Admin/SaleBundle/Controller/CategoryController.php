<?php

namespace Admin\SaleBundle\Controller;

use Admin\SaleBundle\Entity\Category;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 * @Route("category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     *
     * @Route("/", name="category_index")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $criteria = $request->get('criteria');
        $value = $request->get('value');

        $qb = $em->createQueryBuilder()
            ->select('c')
            ->from('AdminSaleBundle:Category', 'c');

        if (!empty($criteria) && !empty($value)) {
            $operator = $request->get('operator');

            $type = $em->getClassMetadata('AdminSaleBundle:Category')->getTypeOfField($criteria);
            if ($type == 'date' || $type == 'datetime') {
                $value = \DateTime::createFromFormat('d/m/Y H:i:s', $value);
            }
            if ($operator === 'like') {
                $value = $qb->expr()->literal('%' . $value . '%');
                $qb->andWhere($qb->expr()->{$operator}('c.' . $criteria, $value));
            } else {
                $qb->andWhere($qb->expr()->{$operator}('c.' . $criteria, '?0'));
                $qb->setParameter(0, $value);
            }
        }

        $paginator = $this->get('knp_paginator');
        $categories = $paginator->paginate(
            $qb->getQuery(),
            $request->get('page', 1)/*page number*/,
            $request->get('limit', 50)/*limit per page*/
        );

        $delete_forms = array_map(function (Category $category) {
            return $this->createDeleteForm($category)->createView();
        }, $categories->getItems());

        return $this->render('AdminSaleBundle:Category:index.html.twig', array(
            'categories' => $categories,
            'delete_forms' => $delete_forms,
        ));
    }

    /**
     * Creates a new category entity.
     *
     * @Route("/new", name="category_new")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm('Admin\SaleBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->get('admin_main.flash')->success('message.success.new');

            return $this->redirectToRoute('category_show', array('id' => $category->getId()));
        }

        return $this->render('AdminSaleBundle:Category:new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a category entity.
     *
     * @Route("/{id}", name="category_show")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);

        return $this->render('AdminSaleBundle:Category:show.html.twig', array(
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/{id}/edit", name="category_edit")
     * @Method({"GET", "POST"})
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Category $category)
    {
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('Admin\SaleBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->get('admin_main.flash')->success('message.success.edit');

            return $this->redirectToRoute('category_edit', array('id' => $category->getId()));
        }

        return $this->render('AdminSaleBundle:Category:edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Deletes a category entity.
     *
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();

            $this->get('admin_main.flash')->success('message.success.delete');
        }

        return $this->redirectToRoute('category_index');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Category $category The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
