<?php

namespace Admin\SaleBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CheckoutControllerTest extends WebTestCase
{
    public function testStepone()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'step-1');
    }

    public function testSteptwo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'step-2');
    }
}
