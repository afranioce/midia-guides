<?php

namespace Admin\SaleBundle\EventListener;

use Admin\SaleBundle\Model\CartItem;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class CartEventListener implements EventSubscriberInterface
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * CartEventListener constructor.
     * @param \Twig_Environment $twig
     * @param SessionInterface $session
     */
    public function __construct(\Twig_Environment $twig, SessionInterface $session)
    {
        $this->twig = $twig;
        $this->session = $session;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => [
                ['onControllerCart']
            ]
        ];
    }

    public function onControllerCart()
    {
        /** @var CartItem[] $cart */
        $cart = $this->session->get('_cart', []);

        // Twig global
        $this->twig->addGlobal('sale_cart', $cart);
    }
}
