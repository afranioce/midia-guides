<?php

namespace Admin\SaleBundle\EventListener;

use Admin\MainBundle\Event\ConfigureMenuEvent;

class ConfigureMenuListener
{
    /**
     * @param \Admin\MainBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $userMenu = $event->getFactory()->createItem('Vendas')
            ->setAttributes(['icon' => 'fa fa-cart']);

        $userMenu->addChild($event->getFactory()
            ->createItem('entity.category', ['route' => 'category_index']));
        $userMenu->addChild($event->getFactory()
            ->createItem('entity.product', ['route' => 'product_index']));
        $userMenu->addChild($event->getFactory()
            ->createItem('entity.order', ['route' => 'order_index']));
        $userMenu->addChild($event->getFactory()
            ->createItem('entity.invoice', ['route' => 'invoice_index']));
        $userMenu->addChild($event->getFactory()
            ->createItem('myorders', ['route' => 'order_my']));

        $menu->addChild($userMenu);
    }
}
