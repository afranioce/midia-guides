<?php

namespace Admin\SaleBundle\EventListener;

use Admin\SaleBundle\Entity\Order;
use Admin\SaleBundle\Entity\OrderStatusHistory;
use Payum\Core\Bridge\Symfony\Event\ExecuteEvent;
use Payum\Core\Bridge\Symfony\PayumEvents;
use Payum\Core\Model\PaymentInterface;
use Payum\Core\Request\BaseGetStatus;
use Payum\Core\Request\Generic;
use Payum\Core\Request\GetHumanStatus;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PaymentListener implements EventSubscriberInterface
{
    /**
     * @var RegistryInterface
     */
    public $registry;

    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            PayumEvents::GATEWAY_POST_EXECUTE => 'onPostExecute'
        ];
    }

    public function onPostExecute(ExecuteEvent $event)
    {
        $request = $event->getContext()->getRequest();

        // Exclude Payum GetStatus request
        // => protect from infinite loop as following code execute one GetStatus request
        if ($request instanceof BaseGetStatus) {
            return;
        }

        // Request types requiring dispatching an event
        if (!($request instanceof Generic)) {
            return;
        }

        $payment = $request->getFirstModel();
        if (!$payment instanceof PaymentInterface) {
            return;
        }

        $order = $this->registry->getRepository(Order::class)->find($payment->getNumber());
        if (!$order instanceof Order) {
            return;
        }

        $gateway = $event->getContext()->getGateway();

        $gateway->execute($status = new GetHumanStatus($payment));

        $orderStatus = new OrderStatusHistory();
        $orderStatus->setOrder($order);
        $orderStatus->setCustomerWasNotified(false);

        if ($status->isCaptured()) {
            $orderStatus
                ->setStatus(Order::STATUS_CAPTURED)
                ->setComment('Pagamento capturado')
                ->setCustomerWasNotified(true);
        } elseif ($status->isPayedout()) {
            $orderStatus
                ->setStatus(Order::STATUS_PAID)
                ->setComment('Pagamento PAGO')
                ->setCustomerWasNotified(true);
        } elseif ($status->isAuthorized()) {
            $orderStatus
                ->setStatus(Order::STATUS_AUTHORIZED)
                ->setComment('Pagamento autorizado')
                ->setCustomerWasNotified(true);
        } elseif ($status->isCanceled()) {
            $orderStatus
                ->setStatus(Order::STATUS_CANCELED)
                ->setComment('Pagamento cancelado');
        } elseif ($status->isPending()) {
            $orderStatus
                ->setStatus(Order::STATUS_PENDING)
                ->setComment('Pagamento pendente');
        } elseif ($status->isRefunded()) {
            $orderStatus
                ->setStatus(Order::STATUS_REFUNDED)
                ->setComment('Pagamento devolvido')
                ->setCustomerWasNotified(true);
        } elseif ($status->isUnknown() || $status->isExpired() || $status->isSuspended()) {
            $orderStatus
                ->setStatus(Order::STATUS_INACTIVE)
                ->setComment('Pagamento inativo');
        } elseif ($status->isNew()) {
            $orderStatus
                ->setStatus(Order::STATUS_PENDING)
                ->setComment('Pedido criado');
        } else {
            $orderStatus
                ->setStatus(Order::STATUS_PENDING)
                ->setComment('Ocorreu algum status inesperado situação: ' . $status->getValue());
        }

        $em = $this->registry->getManager();
        $em->persist($orderStatus);
        $em->flush();
    }
}
