<?php

namespace Admin\SaleBundle\EventListener;

use Admin\SaleBundle\Entity\Invoice;
use Admin\SaleBundle\Event\OrderEvent;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig_Environment;

class OrderSubscriber implements EventSubscriberInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * OrderSubscriber constructor.
     * @param Twig_Environment $twig
     * @param Swift_Mailer $mailer
     */
    public function __construct(Twig_Environment $twig, Swift_Mailer $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            OrderEvent::EMAIL_CREATE => 'doSendEmailCreate',
            OrderEvent::EMAIL_UPDATE => 'doSendEmailUpdate',
            OrderEvent::PAID => 'doCreateInvoice'
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function doSendEmailUpdate(OrderEvent $event)
    {
        $order = $event->getOrder();

        $message = Swift_Message::newInstance()
            ->setSubject('Pedido #' . $order->getId() . ' Atualizado')
            ->setFrom($this->container->getParameter('mailer_noreply'), $this->container->getParameter('site_name'))
            ->setTo($order->getCostumer()->getUser()->getEmail())
            ->setBcc($this->container->getParameter('mailer_bcc_tester'))
            ->setContentType('text/html')
            ->setBody($this->twig->render('AdminSaleBundle:Emails:order_update.html.twig', [
                'order' => $order
            ], 'text/html'));

        $this->mailer->send($message);
    }

    /**
     * @param OrderEvent $event
     */
    public function doSendEmailCreate(OrderEvent $event)
    {
        $order = $event->getOrder();

        $message = Swift_Message::newInstance()
            ->setSubject('Novo Pedido #' . $order->getId())
            ->setFrom($this->container->getParameter('mailer_noreply'), $this->container->getParameter('site_name'))
            ->setTo($order->getCostumer()->getUser()->getEmail())
            ->setBcc($this->container->getParameter('mailer_bcc_tester'))
            ->setContentType('text/html')
            ->setBody($this->twig->render('AdminSaleBundle:Emails:order_new.html.twig', [
                'order' => $order
            ], 'text/html'));

        $this->mailer->send($message);
    }

    public function doCreateInvoice(OrderEvent $event)
    {
        $em = $this->container->get('doctrine')->getManager();

        $order = $event->getOrder();

        if ($order->getInvoice() instanceof Invoice) {
            return;
        }

        $invoice = new Invoice();
        $invoice->setOrder($order);
        $invoice->setTotalAmount($order->getTotalAmount());
        $invoice->setCostumer($order->getCostumer());
        $invoice->setStatus(Invoice::STATUS_ACTIVE);

        $order->setInvoice($invoice);

        $em->persist($invoice);
        $em->flush();

        $message = Swift_Message::newInstance()
            ->setSubject('Fatura #' . $invoice->getId() . ' Gerada')
            ->setFrom($this->container->getParameter('mailer_noreply'), $this->container->getParameter('site_name'))
            ->setTo($invoice->getCostumer()->getUser()->getEmail())
            ->setBcc($this->container->getParameter('mailer_bcc_tester'))
            ->setContentType('text/html')
            ->setBody($this->twig->render('AdminSaleBundle:Emails:invoice_new.html.twig', [
                'invoice' => $invoice
            ], 'text/html'));

        $this->mailer->send($message);
    }
}
