<?php

namespace Admin\SaleBundle\EventListener;

use Admin\SaleBundle\Entity\Order;
use Admin\SaleBundle\Entity\OrderStatusHistory;
use Admin\SaleBundle\Event\OrderEvent;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class OrderStatusHistorySubscriber
 * @package Admin\SaleBundle\EventListener
 */
class OrderStatusHistorySubscriber implements EventSubscriber
{
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof OrderStatusHistory) {
            $order = $entity->getOrder();
            $entity->setCreatedAt(new \DateTime());

            $em = $args->getEntityManager();

            //Verificando se pode atualizar se não for igual ao status anterior
            $old = $em->getRepository(OrderStatusHistory::class)
                ->findOneBy(['order' => $order], ['id' => 'DESC']);

            if ($old instanceof OrderStatusHistory && $old->getStatus() == $entity->getStatus()) {
                return;
            }

            $order->setStatus($entity->getStatus());

            $em->flush($order);

            $event = new OrderEvent($order);

            switch ($entity->getStatus()) {
                case Order::STATUS_AUTHORIZED:
                case Order::STATUS_PAID:
                case Order::STATUS_CAPTURED:
                    $this->dispatcher->dispatch(OrderEvent::PAID, $event);
                    break;
                case Order::STATUS_CANCELED:
                case Order::STATUS_REFUNDED:
                case Order::STATUS_INACTIVE:
                    $this->dispatcher->dispatch(OrderEvent::CANCELED, $event);
                    break;
                case Order::STATUS_PENDING:
                    $this->dispatcher->dispatch(OrderEvent::PENDING, $event);
                    break;
            }

            if ($entity->getCustomerWasNotified()) {
                $this->dispatcher->dispatch(OrderEvent::EMAIL_UPDATE, $event);
            }
        }
    }
}
