<?php

namespace Admin\SaleBundle\Event;

use Admin\SaleBundle\Entity\Order;
use Symfony\Component\EventDispatcher\Event;

class OrderEvent extends Event
{
    const EMAIL_CREATE = 'order.email.create';
    const EMAIL_UPDATE = 'order.email.update';
    const PAID = 'order.status.paid';
    const CANCELED = 'order.status.canceled';
    const PENDING = 'order.status.pending';

    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getOrder()
    {
        return $this->order;
    }
}
