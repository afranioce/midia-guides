<?php

namespace Admin\SaleBundle\Model;

use Admin\SaleBundle\Entity\Product;

/**
 * Class OrderItem
 * @package Admin\SaleBundle\Model
 */
class CartItem implements ItemInterface
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var int
     */
    private $quantity;

    /**
     * Item constructor.
     * @param Product $product
     * @param $quantity
     */
    public function __construct(Product $product, $quantity = 1)
    {
        $this->setQuantity($quantity);
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return CartItem
     */
    public function setQuantity($quantity)
    {
        if (!($quantity = intval($quantity))) {
            throw new \InvalidArgumentException('Invalid item quantity: %s', $quantity);
        }

        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->getProduct()->getUnitPrice() * $this->getQuantity();
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->getProduct()->getUnitPrice();
    }
}
