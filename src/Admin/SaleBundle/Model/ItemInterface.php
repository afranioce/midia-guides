<?php
/**
 * Created by PhpStorm.
 * User: afran
 * Date: 16/04/2017
 * Time: 15:29
 */

namespace Admin\SaleBundle\Model;

use Admin\SaleBundle\Entity\Product;

interface ItemInterface
{
    /**
     * @return Product
     */
    public function getProduct();

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * @return float
     */
    public function getTotal();

    /**
     * @return float
     */
    public function getUnitPrice();
}
