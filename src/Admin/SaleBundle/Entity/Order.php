<?php

namespace Admin\SaleBundle\Entity;

use Admin\ChatBundle\Entity\Thread;
use Admin\MainBundle\Model\Entity as BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="sale_order")
 * @ORM\Entity(repositoryClass="Admin\SaleBundle\Repository\OrdersRepository")
 */
class Order extends BaseEntity
{
    const STATUS_CANCELED = 0X00;
    const STATUS_AUTHORIZED = 0X01;
    const STATUS_APPROVED = 0X02;
    const STATUS_PENDING = 0X03;
    const STATUS_CAPTURED = 0X04;
    const STATUS_REFUNDED = 0X05;
    const STATUS_PAID = 0X06;
    const STATUS_HOLD = 0X07;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivered_at", type="datetime", nullable=true)
     */
    private $deliveredAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="required_at", type="datetime", nullable=true)
     */
    private $requiredAt;

    /**
     * @var Costumer
     *
     * @ORM\ManyToOne(targetEntity="Admin\SaleBundle\Entity\Costumer", cascade={"persist"})
     * @ORM\JoinColumn(name="costumer_id", referencedColumnName="id", nullable=false)
     */
    private $costumer;

    /**
     * @var float
     *
     * @ORM\Column(name="total_amount", type="float", nullable=false)
     */
    private $totalAmount;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Admin\SaleBundle\Entity\OrderItem", mappedBy="order", cascade={"persist"})
     */
    private $items;

    /**
     * @var Invoice
     *
     * @ORM\OneToOne(targetEntity="Admin\SaleBundle\Entity\Invoice", mappedBy="order", cascade={"persist"})
     */
    private $invoice;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Admin\SaleBundle\Entity\OrderStatusHistory", mappedBy="order", cascade={"persist"})
     */
    private $history;

    /**
     * @var Thread
     *
     * @ORM\OneToOne(targetEntity="Admin\ChatBundle\Entity\Thread", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="chat_thread_id", referencedColumnName="id", nullable=false)
     */
    private $talk;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * Set deliveredAt
     *
     * @param \DateTime $deliveredAt
     *
     * @return Order
     */
    public function setDeliveredAt(\DateTime $deliveredAt)
    {
        $this->deliveredAt = $deliveredAt;

        return $this;
    }

    /**
     * Get deliveredAt
     *
     * @return \DateTime
     */
    public function getDeliveredAt()
    {
        return $this->deliveredAt;
    }

    /**
     * Set requiredAt
     *
     * @param \DateTime $requiredAt
     *
     * @return Order
     */
    public function setRequiredAt(\DateTime $requiredAt)
    {
        $this->requiredAt = $requiredAt;

        return $this;
    }

    /**
     * Get requiredAt
     *
     * @return \DateTime
     */
    public function getRequiredAt()
    {
        return $this->requiredAt;
    }

    /**
     * @return Costumer
     */
    public function getCostumer()
    {
        return $this->costumer;
    }

    /**
     * @param Costumer $costumer
     * @return $this
     */
    public function setCostumer(Costumer $costumer = null)
    {
        $this->costumer = $costumer;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     * @return Order
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * @param OrderItem $item
     * @return $this
     */
    public function addItem(OrderItem $item)
    {
        $this->items->add($item);

        return $this;
    }

    /**
     * @param $item
     */
    public function removeItem(OrderItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * @return OrderItem[]|ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param Invoice $invoice
     * @return $this
     */
    public function setInvoice(Invoice $invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * @param $statusHistory
     */
    public function removeHistory(OrderStatusHistory $statusHistory)
    {
        $this->items->removeElement($statusHistory);
    }

    /**
     * @param OrderStatusHistory $statusHistory
     * @return Order
     */
    public function addHistory(OrderStatusHistory $statusHistory)
    {
        $this->history->add($statusHistory);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getHistories()
    {
        return $this->history;
    }

    /**
     * @return Thread
     */
    public function getTalk()
    {
        return $this->talk;
    }

    /**
     * @param Thread $talk
     * @return Order
     */
    public function setTalk($talk)
    {
        $this->talk = $talk;

        return $this;
    }

    public function getStatusList()
    {
        return [
                Order::STATUS_CANCELED => 'Canceled',
                Order::STATUS_AUTHORIZED => 'Authorized',
                Order::STATUS_CAPTURED => 'Captured',
                Order::STATUS_REFUNDED => 'Refunded',
                Order::STATUS_PAID => 'Paid',
                Order::STATUS_APPROVED => 'Approved',
                Order::STATUS_PENDING => 'Pending',
                Order::STATUS_HOLD => 'Hold'
            ] + parent::getStatusList();
    }

    public static function getStatusStaticList()
    {
        return [
                Order::STATUS_CANCELED => 'Canceled',
                Order::STATUS_AUTHORIZED => 'Authorized',
                Order::STATUS_CAPTURED => 'Captured',
                Order::STATUS_REFUNDED => 'Refunded',
                Order::STATUS_PAID => 'Paid',
                Order::STATUS_APPROVED => 'Approved',
                Order::STATUS_PENDING => 'Pending',
                Order::STATUS_HOLD => 'Hold'
            ];
    }
}
