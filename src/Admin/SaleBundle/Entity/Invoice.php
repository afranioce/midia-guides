<?php

namespace Admin\SaleBundle\Entity;

use Admin\MainBundle\Model\Entity as BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="sale_invoice")
 * @ORM\Entity(repositoryClass="Admin\SaleBundle\Repository\InvoiceRepository")
 */
class Invoice extends BaseEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="required_at", type="datetime", nullable=true)
     */
    private $requiredAt;

    /**
     * @var Costumer
     *
     * @ORM\ManyToOne(targetEntity="Admin\SaleBundle\Entity\Costumer", cascade={"persist"})
     * @ORM\JoinColumn(name="costumer_id", referencedColumnName="id", nullable=false)
     */
    private $costumer;

    /**
     * @var float
     *
     * @ORM\Column(name="total_amount", type="float", nullable=false)
     */
    private $totalAmount;

    /**
     * @var Order
     *
     * @ORM\OneToOne(targetEntity="Admin\SaleBundle\Entity\Order", inversedBy="invoice", cascade={"persist"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * @return \DateTime
     */
    public function getRequiredAt()
    {
        return $this->requiredAt;
    }

    /**
     * @param \DateTime $requiredAt
     * @return $this
     */
    public function setRequiredAt(\DateTime $requiredAt)
    {
        $this->requiredAt = $requiredAt;

        return $this;
    }

    /**
     * @return Costumer
     */
    public function getCostumer()
    {
        return $this->costumer;
    }

    /**
     * @param Costumer $costumer
     * @return $this
     */
    public function setCostumer(Costumer $costumer)
    {
        $this->costumer = $costumer;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * @param float $totalAmount
     * @return $this
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }
}
