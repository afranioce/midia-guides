<?php

namespace Admin\SaleBundle\Entity;

use Admin\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Costumer
 *
 * @ORM\Table(name="sale_costumer")
 * @ORM\Entity(repositoryClass="Admin\SaleBundle\Repository\CostumerRepository")
 */
class Costumer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Admin\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }
}
