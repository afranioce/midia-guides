<?php

namespace Admin\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderStatusHistory
 *
 * @ORM\Table(name="sale_order_status_history")
 * @ORM\Entity(repositoryClass="Admin\SaleBundle\Repository\OrderStatusHistoryRepository")
 */
class OrderStatusHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="customer_was_notified", type="boolean", nullable=true)
     */
    private $customerWasNotified;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Admin\SaleBundle\Entity\Order", inversedBy="history")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerWasNotified
     *
     * @param boolean $customerWasNotified
     *
     * @return OrderStatusHistory
     */
    public function setCustomerWasNotified($customerWasNotified)
    {
        $this->customerWasNotified = $customerWasNotified;

        return $this;
    }

    /**
     * Get customerWasNotified
     *
     * @return bool
     */
    public function getCustomerWasNotified()
    {
        return $this->customerWasNotified;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return OrderStatusHistory
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return OrderStatusHistory
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return OrderStatusHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order = null)
    {
        $this->order = $order;

        return $this;
    }
}
