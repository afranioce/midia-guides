<?php

namespace Admin\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payum\Core\Model\Token;

/**
 * @ORM\Table(name="sale_payment_token")
 * @ORM\Entity(repositoryClass="Admin\SaleBundle\Repository\PaymentTokenRepository")
 */
class PaymentToken extends Token
{
}
