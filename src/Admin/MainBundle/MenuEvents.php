<?php

namespace Admin\MainBundle;

final class MenuEvents
{
    /**
     * @var string
     */
    const CONFIGURE_MAIN = 'admin_main.menu.configure_main';

    /**
     * @var string
     */
    const CONFIGURE_SETTINGS = 'admin_main.menu.configure_settings';

    /**
     * @var string
     */
    const CONFIGURE_USER = 'admin_main.menu.configure_user';
}
