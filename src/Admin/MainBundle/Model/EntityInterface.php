<?php

namespace Admin\MainBundle\Model;

use Admin\UserBundle\Entity\User;
use DateTime;

interface EntityInterface
{
    const STATUS_ACTIVE = 0X01;
    const STATUS_INACTIVE = 0X00;

    /**
     * Returns the user unique id.
     *
     * @return integer
     */
    public function getId();

    /**
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * @param DateTime $date
     * @return self
     */
    public function setCreatedAt(DateTime $date);

    /**
     * @return DateTime
     */
    public function getChangedAt();

    /**
     * @param DateTime|null $date
     *
     * @return self
     */
    public function setChangedAt(DateTime $date = null);

    /**
     * @return User
     */
    public function getCreatedBy();

    /**
     * @param User $user
     * @return self
     */
    public function setCreatedBy(User $user);

    /**
     * @return User
     */
    public function getChangedBy();

    /**
     * @param User|null $user
     * @return self
     */
    public function setChangedBy(User $user = null);

    /**
     * @return integer
     */
    public function getStatus();

    /**
     * @param integer $status
     * @return self
     */
    public function setStatus($status = self::STATUS_ACTIVE);
}
