<?php

namespace Admin\MainBundle\Model;

use Admin\UserBundle\Entity\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Transliterator;

abstract class Entity implements EntityInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $changedAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Admin\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", nullable=false)
     */
    protected $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Admin\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="changed_by_id", referencedColumnName="id", nullable=true)
     */
    protected $changedBy;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    protected $status;


    /**
     * Returns the user unique id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $date
     * @return self
     */
    public function setCreatedAt(DateTime $date)
    {
        $this->createdAt = $date;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getChangedAt()
    {
        return $this->changedAt;
    }

    /**
     * @param DateTime|null $date
     *
     * @return self
     */
    public function setChangedAt(DateTime $date = null)
    {
        $this->changedAt = $date;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $user
     * @return self
     */
    public function setCreatedBy(User $user)
    {
        $this->createdBy = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getChangedBy()
    {
        return $this->changedBy;
    }

    /**
     * @param User|null $user
     * @return self
     */
    public function setChangedBy(User $user = null)
    {
        $this->changedBy = $user;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return self
     */
    public function setStatus($status = self::STATUS_ACTIVE)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @todo ainda não está em uso
     * Transform (e.g. "Hello World") into a slug (e.g. "hello-world").
     *
     * @param string $string
     *
     * @return string
     */
    public function slugify($string)
    {
        $rule = 'NFD; [:Nonspacing Mark:] Remove; NFC';
        $transliterator = Transliterator::create($rule);
        $string = $transliterator->transliterate($string);

        return preg_replace(
            '/[^a-z0-9]/',
            '-',
            strtolower(trim(strip_tags($string)))
        );
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            Entity::STATUS_ACTIVE => 'Active',
            Entity::STATUS_INACTIVE => 'Inactive'
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getId();
    }
}
