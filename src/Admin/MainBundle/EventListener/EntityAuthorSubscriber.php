<?php

namespace Admin\MainBundle\EventListener;

use Admin\MainBundle\Model\Entity;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class EntityAuthorSubscriber
 * @package Admin\MainBundle\EventListener
 */
class EntityAuthorSubscriber implements EventSubscriber
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
            Events::preUpdate,
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Entity) {
            if (!$entity->getCreatedBy()) {
                $entity->setCreatedBy($this->tokenStorage->getToken()->getUser());
            }
            $entity->setCreatedAt(new \DateTime());
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Entity) {
            if (!$entity->getChangedBy()) {
                $entity->setChangedBy($this->tokenStorage->getToken()->getUser());
            }
            $entity->setChangedAt(new \DateTime());
        }
    }
}
