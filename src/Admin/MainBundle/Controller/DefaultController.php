<?php

namespace Admin\MainBundle\Controller;

use Admin\ProjectBundle\Entity\Project;
use Doctrine\ORM\EntityManager;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/dashboard", name="admin_main_homepage")
     * @param Request $request
     * @Secure(roles="ROLE_USER")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $paginator = $this->get('knp_paginator');
        $projects = $paginator->paginate(
            $em->getRepository(Project::class)->getQueryFindByUser($this->getUser()),
            $request->get('page', 1)/*page number*/,
            $request->get('limit', 50)/*limit per page*/
        );

        return $this->render('AdminProjectBundle:Default:index.html.twig', [
            'projects' => $projects
        ]);
    }
}
