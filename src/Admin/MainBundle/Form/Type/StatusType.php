<?php

namespace Admin\MainBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatusType extends AbstractType
{
    private $statusChoices;

    public function __construct(array $statusChoices)
    {
        $this->statusChoices = $statusChoices;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array_flip($this->statusChoices),
            'expanded' => true,
            'placeholder' => 'Choose a status',
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
